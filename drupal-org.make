; Drupal.org release file.
core = 7.10
api = 2

projects[addressfield] = 1.0-beta2
projects[admin_menu] = 3.0-rc1
projects[admin_path] = 1.0
projects[commerce] = 1.2
projects[commerce_google_analytics] = 1.0-rc2
projects[ctools] = 1.0-rc1
projects[date] = 2.0-rc1
projects[defaultcontent] = 1.0-alpha4
projects[diff] = 2.0
projects[entity] = 1.0-rc1
projects[features] = 1.0-beta6
projects[google_analytics] = 1.2
projects[jstimer] = 1.3
projects[module_filter] = 1.6
projects[pathauto] = 1.0
projects[pathologic] = 1.4
projects[references] = 2.0
projects[rules] = 2.0
projects[stringoverrides] = 1.8
projects[strongarm] = 2.0-beta5
projects[token] = 1.0-beta7
projects[views] = 3.1
projects[webform] = 3.15
projects[wysiwyg] = 2.1
projects[xmlsitemap] = 2.0-rc1
